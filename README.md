## Créer un nouveau projet basé sur Mycelia
- *(si pas déjà dispo sur l'ordinateur)* installer git en version 2.3 minimum
- *(si pas déjà dispo sur l'ordinateur)* installer nodejs (v8 ou plus) avec npm
- *(si pas déjà dispo sur l'ordinateur)* installer mycelia-cli : tapper `npm install -g mycelia-cli` en ligne de commande (git-bash pour les utilisateurs windows)
- dans le dossier ou vous voulez mettre votre nouveau projet :
  - `mycelia init` pour le créer sur place
  - ou `mycelia init monFabuleuxProjet` pour le créer dans un sous dossier à son nom.

- si, en tant que membre d'AGATAs, vous utilisez framagit.org/AGATAs
  - créez-y un projet vide au nom de votre projet (en suivant les instructions ci-dessus).
  - assurez-vous d'avoir votre clef ssh¹ accessible sur votre machine et la clef publique partagé sur framagit.
  - Si vous avez les droits pour publier sur un serveur distant, ajoutez dans framagit la clef de publication dans les secrets de CI du projet https://framagit.org/AGATAs/insererIciLeNomDuProjet/settings/ci_cd .
    *PS : clef de plublication -> de même que vous avez une clef privée personnelle reconnue par framagit via votre clef publique, pour vous authentifier et pouvoir faire des commit et des push, framagit a besoin d'une clef privée reconnue par le serveur de publication pour pouvoir s'authentifier dessus pour y mettre le projet mis à jour. Cette clef peut être copiée depuis un projet existant s'il est destiné à être hébergé au même endroit.
    PS2 : CI -> continuous integration (intégration continue) voir wikipedia pour plus de détails.*
  - faite un git push depuis le dossier de votre projet.

- si vous n'utilisez pas l'infrastructure d'AGATAs
  - changez l'origin git pour votre dépos distant.
  - adaptez les scripts de CI a votre infrastructure.

¹ créer et configurer sa clef ssh :
- `ssh-keygen -t rsa -C "nom utilisateur et nom machine ex: Millicent PC portable"` puis faire entrer à chaque question sans rien mettre.
- `cat ~/.ssh/id_rsa.pub` copier les quelques lignes qui s'affiche (de `ssh-rsa` jusqu'au nom que vous avez donné à la clef.)
- rendez vous sur framagit, Settings, SSH Keys : https://framagit.org/profile/keys
- collez la clef dans la zone `key` puis valider en appuyant sur `add key`. 

**⚠ Les dépos git stockant les données d'un projet mycelia contienne des données sensibles (configuration du server email, emails des utilisateurs...).
Ces dépos git ne sont donc pas destinés à être accessible publiquement.**

## Lancer une instance locale
- Initialiser (avec `mycelia init` comme vu plus haut)
- Ou charger un projet distant avec un git clone
- dans le dossier du projet pour les utilsiateurs windows :
  - `mycelia build` (après chaque modification, pour pouvoir accéder au projet actualisé en lançant via le fichier generated.build.public/index.html)
- dans le dossier du projet pour les utilsiateurs linux ou mac :
  - `mycelia start` (après chaque redémarrage de la machine, pour le projet sur le quel travailler en local (1 seul à la fois)) ou, `mycelia contribAuto` si vous avez configuré votre clef ssh¹ pour commit pull push automatiquement vos modificiation en plus d'actualiser automatiquement l'application en local.
  

PS : si vous avez correctement configurez votre instance local et votre clef ssh, les modifications sauvegardé depuis l'interface de mycelia serons automatiquement fusionné dans le dépos git de référence (framagit.org/AGATAs/nomProjet pour les projets AGATAs)
PS2 : les modifications faites à la mains sont à commit et push manuelement, mais une fois push, elles s'appliquerons à toutes les instances local et en productions qui se syncronnisent sur le dépos de référence.

## Différence entre le fonctionnement statique et la version propulsé par mycelia-cli
- `allData/` deviens `public/`
- `allData/publicData.yml` deviens `data.yml` (en dehors du dossier `public/`)
- le index.html qui était à la racine du projet est désormais dans `generated.build.public/index.html` après avoir lancé `mycelia build`
- pour travailler sur un projet en local, au lieu du `npm install`, `npm start`, c'est `mycelia build` qu'il faut executer sous windows dans le dossier du projet après chaque modification faite dans le dossier (export de données, css/stylus, picto, config...).
- pour les utilisateur linux, plus d'automatisme avec `mycelia contribAuto` a lancer une seul fois par redémarrage sur le projet en cours pour faire les commit, pull et push automatiquement. (nécessite d'avoir cloné le projet en avec l'adresse ssh et non celle en https, et nécessite aussi d'avoir bien configuré sa clef ssh¹.)
- changer le titre du projet affiché dans l'interface de mycelia ce fait désormais dans la trad.
- changer les calques de légende qui servent à mettre les icones dans le graph se fait depuis `public/config.yml` via l'option `layersPicto2GraphRegExFilter` et non plus en vrac dans `graph.js`

## notes

- mycelia-cli init : cré l'environnement minimal pour une nouvelle instance mycelia dans le dossier courant.
  - copie du scaffold + git init si pas de .git
  - TODO: assitant au scaffolding (voulez-vous activer les fonctions d'éditions (et le serveur d'édition collaborative) ? voulez-vous désactiver les sauvegardes de versions ?

- mycelia-cli update : met à jour les dépendances du projet (mycelia-cli, mycelia-server-nodejs, mycelia-front-app)

- mycelia-cli build : assemble données et application statique dans un dossier generated.prod
  - copie l'application en version prod
  - copie les données statiques
  - copie data.yml, le filtre selon les droits d'accès et y ajoute le sha1 de la version git

- mycelia-cli start : lance un serveur statique sur le dossier build et un serveur d'édition relié aux données + lance le navigateur avec mycelia-front-app relié au serveur d'édition.
  - cd node_modules/mycelia-server-nodejs/ && npm start ../../ 55025
  - récupère localhost:port où tourne le serveur
  - cd generated.prod && npm start --edit-srv=localhost:port (55025)
- mycelia-cli watch : mycelia-cli start + aux changement de données, refait un build
  - watch ./ --> mycelia-cli build
  - mycelia-cli start
