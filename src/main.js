const fs = require('fs-extra');
const git = require('simple-git/promise');
const yaml = require('js-yaml');
const watch = require('glob-watcher');
const readline = require('readline');
const latinize = require('latinize');
const npm = require('check-dependencies');
const tar = require('tar');

async function ask(question){
	return new Promise( (resolve,reject) => {
		const rl = readline.createInterface({input: process.stdin, output: process.stdout});
		rl.question(question, (answer) => {
			resolve(answer);
			rl.close();
		});
	});
}

async function help(osef,command) {
	console.log(`\nAvailable actions : ${Object.keys(module.exports).join(', ')}\n`);
	console.log('mycelia-cli init -> init a new mycelia project');
	console.log('mycelia-cli update -> update project dependencies');
	console.log('mycelia-cli start -> start a fully functionnal local instance');
	console.log('mycelia-cli build -> build a static instance usable from browsers');
	console.log('mycelia-cli contribAuto -> like start with auto commit pull push');

	console.log('\nSee https://framagit.org/mycelia/mycelia-cli for more.\n');
}
async function version(path){
	const pkg = require('../package.json');
	console.log(pkg.version);
}
async function init(path, projectName = "myceliaProject") {
	console.log('Initialization start...');
	await fs.ensureDir(path);
	if((await fs.readdir(path)).length){
		if(projectName === "myceliaProject") projectName = await ask('Input project name (without space) : ');
		await init(`${path}/${projectName}`,projectName);
		return 0;
	} else {
		if(projectName === "myceliaProject") projectName = path.split('/').pop().split('\\').pop();
	}
	console.log(`Project folder : ${path}`);
	console.log(`ProjectName : ${projectName}`);
	/* TODO: an interactive wizzard bypassable with default and cli args
	project name ?
	edition server ?
	if no edition server : versioning ?
	if edition server : versioning = yes
	if edition server : admin email ?
	if versioning : git distant repository uri ?
	check for ssh key to push in distant repos and guide if missing.
	ask for email config
	*/
	console.log('Scaffold project from default template...');
	if(await fs.pathExists(`${__dirname}/../template.tgz`)){
		await tar.extract({file: `${__dirname}/../template.tgz`,cwd:path});
	}else await fs.copy(`${__dirname}/../projectTemplate`,path,{dereference:true});
	console.log('Generate random secret for secure authentication...');
	const configSrv = await yaml.safeLoad(await fs.readFile(`${path}/config-srv.yml`, 'utf8'));
	configSrv.secretKey = require('crypto').randomBytes(256).toString('base64');
	await fs.writeFile(`${path}/config-srv.yml`,await yaml.safeDump(configSrv));
	console.log('Insert project name in .gitlab-ci.yml ...');
	const ciConf = await yaml.safeLoad(await fs.readFile(`${path}/.gitlab-ci.yml`, 'utf8'));
	ciConf.variables.PROJECT_NAME = projectName;
	await fs.writeFile(`${path}/.gitlab-ci.yml`,await yaml.safeDump(ciConf));
	await update(path);
	console.log('Initialize versioning with git...');
	const gitRepos = git(path);
	await gitRepos.init();
	await gitRepos.addConfig('receive.denyCurrentBranch','updateInstead');
	await gitRepos.addConfig('user.email','mycelia-cli@mycelia.cli');
	await gitRepos.addConfig('user.name','mycelia-cli');
	await gitRepos.add(['.']);
	await gitRepos.commit('init');
	await gitRepos.addRemote('origin',`git@framagit.org:AGATAs/${projectName}.git`); //FIXME : customisable git origin
	console.log(`Init complete.`);
	console.log(`Check config-srv.yml and users.yml at least.`);
	console.log(`You should also check .gitlab-ci.yml and customize public folder content.`);
}
async function update(path) {
	const ncu = require('npm-check-updates');
	console.log('Update project dependencies if needed.');
	await ncu.run({
		packageFile: `${path}/package.json`,
		upgrade: true,
	});
	console.log('Update project dependencies done.');
}
async function build(path) {
	console.log('Building the front-end app with project data and configuration...');
	try{
		if( "mycelia start" !== (await fs.readJson(`${path}/package.json`)).scripts.start)
			throw 'mycelia build fail : invalid project';
	} catch (e){
		throw `mycelia build fail : not a mycelia project (${path})`;
	}
	console.log('Ensure project dependencies availability...');
	await npm({install:true,scopeList:['dependencies'],packageDir:path});
	console.log('Copy app -> build folder');
	const projectPkg = 	await fs.readJson(`${path}/package.json`);
	const localDep = projectPkg.dependencies["mycelia-front-app"].substr(0,5)==="file:";
	await fs.ensureDir(`${path}/generated.build.public/allData`);
	if(!localDep) await fs.copy(`${path}/node_modules/mycelia-front-app`, `${path}/generated.build.public`,{dereference:true});
	await fs.remove(`${path}/generated.build.public/allData`);
	console.log('Copy assets -> build folder (project specific config, css, images...)');
	await fs.copy(`${path}/public`,`${path}/generated.build.public/allData`,{dereference:true});
	console.log('Transpile stylus customStyle to css');
	const stylBase = `${path}/generated.build.public/allData/customStyle`;
	await stylus2css(`${stylBase}.styl`,`${stylBase}.css`);
	console.log('Copy data -> build folder (data with current git commit version marker)');
	const data = await yaml.safeLoad(await fs.readFile(`${path}/data.yml`, 'utf8')) || {};
	data.commitHash = (await git(path).silent(true).revparse(['HEAD'])).trim();
	await fs.writeFile(`${path}/generated.build.public/allData/cache.json`,JSON.stringify({
		appVersion:projectPkg.dependencies["mycelia-front-app"],
		commitHash:data.commitHash,
		commitNumber: (await git(path).silent(true).log(['HEAD'])).all.length
	}));
	await fs.writeFile(`${path}/generated.build.public/allData/data.yml`,await yaml.safeDump(data));
	console.log('Build successfully finished.');
}
async function stylus2css(fromFile,toFile) {
	return new Promise( async (resolve,reject) => {
		const stylus = require('stylus');
		stylus.render(
			await fs.readFile(fromFile, 'utf8'),
			{ filename: fromFile },
			async (err, css) => {
				if (err) reject(err);
				await fs.writeFile(toFile,css);
				resolve();
		});
	});
}
async function startMyceliaServer(options) {
	const myceliaServerModule = require(`${options.dataFolder}node_modules/mycelia-server-nodejs`);
	return await myceliaServerModule.start(options);
}
async function startStaticServer(path,port) {
	const projectPkg = await fs.readJson(`${path}/../package.json`);
	const localDep = projectPkg.dependencies["mycelia-front-app"].substr(0,5)==="file:";
	console.log(localDep);
	if(localDep) return new Promise( (resolve,reject) => {
		const nStatic = require('node-static');
		const dataServer = new nStatic.Server(path);
		const appServer = new nStatic.Server(`${path}/../${projectPkg.dependencies["mycelia-front-app"].substr(5)}`);
		const http = require('http');
		const srv = http.createServer((request, response)=>
			request.addListener('end', ()=>{
				if(request.url.indexOf("allData")!== -1) return dataServer.serve(request, response);
				else return appServer.serve(request, response);
			} ).resume()
		);
		srv.listen(port, ()=> {
			resolve(srv); //srv.address().port "http://localhost:"+srv.address().port+"/"
		});
	});
	else return new Promise( (resolve,reject) => {
		const nStatic = require('node-static');
		const fileServer = new nStatic.Server(path);
		const http = require('http');
		const srv = http.createServer((request, response)=>
			request.addListener('end', ()=>fileServer.serve(request, response) ).resume()
		);
		srv.listen(port, ()=> {
			resolve(srv); //srv.address().port "http://localhost:"+srv.address().port+"/"
		});
	});
}
async function startLinearPart(path){
	console.log('Check dependencies installation (and install them if needed)...');
	await npm({install:true,scopeList:['dependencies'],packageDir:path});
	console.log('Dependencies ok !');

	console.log('Building the front-end app with project data and configuration...');
	await build(path);
	console.log('Build complete.');

	console.log('Start local http server to expose the front-end...');
	const frontSrv = await startStaticServer(`${path}/generated.build.public`); // ,55080
	const frontPort = frontSrv.address().port;

	console.log('Start local edition server to expose the api used for login and persistence...');
	const srvOptions = {
		dataFolder:`${path}/`,
		allowedDomains:[`http://localhost:${frontPort}`],
		logEmailToken:true
	};
	const backSrv = await startMyceliaServer(srvOptions);
	const srvPort = backSrv.hapiSrv.info.port;

	const url= `http://localhost:${frontPort}/#{"persistanceAPI":"http://localhost:${srvPort}"}`;
	console.log(`Mycelia front-end is now locally available here : ${url}`);
	const opn = require('opn');
	opn(url.replace(/"/g,'%22'));
}
async function start(path) {
	await startLinearPart(path);
	watch([`${path}/!(node_modules|generated*)`,`${path}/public/**/*`],{delay:500}, async function(done){//FIXME: handle no-public folders
		console.log('Project files changed.');
		console.log('Rebuilding the front-end with new data/configuration...');
		await build(path);
		//TODO: si config autoUpdate != false mettre à jour les dépendances du projet puis relancer
		console.log('Rebuilding complete.');
		done();
	});
}
async function gitSync(path) {
	const gitRepos = git(path);
	await gitRepos.addConfig('receive.denyCurrentBranch','updateInstead');
	await gitRepos.raw(["rm","-rf","--cached","."]);
	await gitRepos.add(['.']);
	try{await gitRepos.raw(["rm","--cached","package-lock.json"]);}catch (e){}
	await gitRepos.status();
	await gitRepos.commit('auto-commit');
	await gitRepos.pull('origin', 'master');
	await gitRepos.push('origin', 'master');
}
async function contribAuto(path) {
	try{await fs.remove(`${path}/package-lock.json`);}catch (e){}
	await gitSync(path);
	await update(path);
	await gitSync(path);
	await startLinearPart(path);
	try{await fs.remove(`${path}/package-lock.json`);}catch (e){}
	watch([`${path}/!(node_modules|generated*)`,`${path}/public/**/*`],{queue:false}, async function(done){//FIXME: handle no-public folders
		console.log('Project files changed.');
		console.log('Rebuilding the front-end with new data/configuration...');
		await build(path);
		console.log('Rebuilding complete.');
		gitSync(path);
		done();
	});
}
function san(string,sanType){
	const cleanString = latinize(string.toString().trim());
	switch(sanType){
		//"# Aà", "#-Aa", "#Aa" ou "#a"
		case " Aà":
			const conversionMap = {'Á':'A','Ă':'A','Ắ':'A','Ặ':'A','Ằ':'A','Ẳ':'A','Ẵ':'A','Ǎ':'A','Â':'A','Ấ':'A','Ậ':'A','Ầ':'A','Ẩ':'A','Ẫ':'A','Ä':'A','Ǟ':'A','Ȧ':'A','Ǡ':'A','Ạ':'A','Ȁ':'A','À':'A','Ả':'A','Ȃ':'A','Ā':'A','Ą':'A','Å':'A','Ǻ':'A','Ḁ':'A','Ⱥ':'A','Ã':'A','Ꜳ':'AA','Æ':'AE','Ǽ':'AE','Ǣ':'AE','Ꜵ':'AO','Ꜷ':'AU','Ꜹ':'AV','Ꜻ':'AV','Ꜽ':'AY','Ḃ':'B','Ḅ':'B','Ɓ':'B','Ḇ':'B','Ƀ':'B','Ƃ':'B','Ć':'C','Č':'C','Ç':'C','Ḉ':'C','Ĉ':'C','Ċ':'C','Ƈ':'C','Ȼ':'C','Ď':'D','Ḑ':'D','Ḓ':'D','Ḋ':'D','Ḍ':'D','Ɗ':'D','Ḏ':'D','ǲ':'D','ǅ':'D','Đ':'D','Ƌ':'D','Ǳ':'DZ','Ǆ':'DZ','É':'E','Ĕ':'E','Ě':'E','Ȩ':'E','Ḝ':'E','Ê':'E','Ế':'E','Ệ':'E','Ề':'E','Ể':'E','Ễ':'E','Ḙ':'E','Ë':'E','Ė':'E','Ẹ':'E','Ȅ':'E','È':'E','Ẻ':'E','Ȇ':'E','Ē':'E','Ḗ':'E','Ḕ':'E','Ę':'E','Ɇ':'E','Ẽ':'E','Ḛ':'E','Ꝫ':'ET','Ḟ':'F','Ƒ':'F','Ǵ':'G','Ğ':'G','Ǧ':'G','Ģ':'G','Ĝ':'G','Ġ':'G','Ɠ':'G','Ḡ':'G','Ǥ':'G','Ḫ':'H','Ȟ':'H','Ḩ':'H','Ĥ':'H','Ⱨ':'H','Ḧ':'H','Ḣ':'H','Ḥ':'H','Ħ':'H','Í':'I','Ĭ':'I','Ǐ':'I','Î':'I','Ï':'I','Ḯ':'I','İ':'I','Ị':'I','Ȉ':'I','Ì':'I','Ỉ':'I','Ȋ':'I','Ī':'I','Į':'I','Ɨ':'I','Ĩ':'I','Ḭ':'I','Ꝺ':'D','Ꝼ':'F','Ᵹ':'G','Ꞃ':'R','Ꞅ':'S','Ꞇ':'T','Ꝭ':'IS','Ĵ':'J','Ɉ':'J','Ḱ':'K','Ǩ':'K','Ķ':'K','Ⱪ':'K','Ꝃ':'K','Ḳ':'K','Ƙ':'K','Ḵ':'K','Ꝁ':'K','Ꝅ':'K','Ĺ':'L','Ƚ':'L','Ľ':'L','Ļ':'L','Ḽ':'L','Ḷ':'L','Ḹ':'L','Ⱡ':'L','Ꝉ':'L','Ḻ':'L','Ŀ':'L','Ɫ':'L','ǈ':'L','Ł':'L','Ǉ':'LJ','Ḿ':'M','Ṁ':'M','Ṃ':'M','Ɱ':'M','Ń':'N','Ň':'N','Ņ':'N','Ṋ':'N','Ṅ':'N','Ṇ':'N','Ǹ':'N','Ɲ':'N','Ṉ':'N','Ƞ':'N','ǋ':'N','Ñ':'N','Ǌ':'NJ','Ó':'O','Ŏ':'O','Ǒ':'O','Ô':'O','Ố':'O','Ộ':'O','Ồ':'O','Ổ':'O','Ỗ':'O','Ö':'O','Ȫ':'O','Ȯ':'O','Ȱ':'O','Ọ':'O','Ő':'O','Ȍ':'O','Ò':'O','Ỏ':'O','Ơ':'O','Ớ':'O','Ợ':'O','Ờ':'O','Ở':'O','Ỡ':'O','Ȏ':'O','Ꝋ':'O','Ꝍ':'O','Ō':'O','Ṓ':'O','Ṑ':'O','Ɵ':'O','Ǫ':'O','Ǭ':'O','Ø':'O','Ǿ':'O','Õ':'O','Ṍ':'O','Ṏ':'O','Ȭ':'O','Ƣ':'OI','Ꝏ':'OO','Ɛ':'E','Ɔ':'O','Ȣ':'OU','Ṕ':'P','Ṗ':'P','Ꝓ':'P','Ƥ':'P','Ꝕ':'P','Ᵽ':'P','Ꝑ':'P','Ꝙ':'Q','Ꝗ':'Q','Ŕ':'R','Ř':'R','Ŗ':'R','Ṙ':'R','Ṛ':'R','Ṝ':'R','Ȑ':'R','Ȓ':'R','Ṟ':'R','Ɍ':'R','Ɽ':'R','Ꜿ':'C','Ǝ':'E','Ś':'S','Ṥ':'S','Š':'S','Ṧ':'S','Ş':'S','Ŝ':'S','Ș':'S','Ṡ':'S','Ṣ':'S','Ṩ':'S','Ť':'T','Ţ':'T','Ṱ':'T','Ț':'T','Ⱦ':'T','Ṫ':'T','Ṭ':'T','Ƭ':'T','Ṯ':'T','Ʈ':'T','Ŧ':'T','Ɐ':'A','Ꞁ':'L','Ɯ':'M','Ʌ':'V','Ꜩ':'TZ','Ú':'U','Ŭ':'U','Ǔ':'U','Û':'U','Ṷ':'U','Ü':'U','Ǘ':'U','Ǚ':'U','Ǜ':'U','Ǖ':'U','Ṳ':'U','Ụ':'U','Ű':'U','Ȕ':'U','Ù':'U','Ủ':'U','Ư':'U','Ứ':'U','Ự':'U','Ừ':'U','Ử':'U','Ữ':'U','Ȗ':'U','Ū':'U','Ṻ':'U','Ų':'U','Ů':'U','Ũ':'U','Ṹ':'U','Ṵ':'U','Ꝟ':'V','Ṿ':'V','Ʋ':'V','Ṽ':'V','Ꝡ':'VY','Ẃ':'W','Ŵ':'W','Ẅ':'W','Ẇ':'W','Ẉ':'W','Ẁ':'W','Ⱳ':'W','Ẍ':'X','Ẋ':'X','Ý':'Y','Ŷ':'Y','Ÿ':'Y','Ẏ':'Y','Ỵ':'Y','Ỳ':'Y','Ƴ':'Y','Ỷ':'Y','Ỿ':'Y','Ȳ':'Y','Ɏ':'Y','Ỹ':'Y','Ź':'Z','Ž':'Z','Ẑ':'Z','Ⱬ':'Z','Ż':'Z','Ẓ':'Z','Ȥ':'Z','Ẕ':'Z','Ƶ':'Z','Ĳ':'IJ','Œ':'OE','ᴀ':'A','ᴁ':'AE','ʙ':'B','ᴃ':'B','ᴄ':'C','ᴅ':'D','ᴇ':'E','ꜰ':'F','ɢ':'G','ʛ':'G','ʜ':'H','ɪ':'I','ʁ':'R','ᴊ':'J','ᴋ':'K','ʟ':'L','ᴌ':'L','ᴍ':'M','ɴ':'N','ᴏ':'O','ɶ':'OE','ᴐ':'O','ᴕ':'OU','ᴘ':'P','ʀ':'R','ᴎ':'N','ᴙ':'R','ꜱ':'S','ᴛ':'T','ⱻ':'E','ᴚ':'R','ᴜ':'U','ᴠ':'V','ᴡ':'W','ʏ':'Y','ᴢ':'Z','á':'a','ă':'a','ắ':'a','ặ':'a','ằ':'a','ẳ':'a','ẵ':'a','ǎ':'a','â':'a','ấ':'a','ậ':'a','ầ':'a','ẩ':'a','ẫ':'a','ä':'a','ǟ':'a','ȧ':'a','ǡ':'a','ạ':'a','ȁ':'a','à':'a','ả':'a','ȃ':'a','ā':'a','ą':'a','ᶏ':'a','ẚ':'a','å':'a','ǻ':'a','ḁ':'a','ⱥ':'a','ã':'a','ꜳ':'aa','æ':'ae','ǽ':'ae','ǣ':'ae','ꜵ':'ao','ꜷ':'au','ꜹ':'av','ꜻ':'av','ꜽ':'ay','ḃ':'b','ḅ':'b','ɓ':'b','ḇ':'b','ᵬ':'b','ᶀ':'b','ƀ':'b','ƃ':'b','ɵ':'o','ć':'c','č':'c','ç':'c','ḉ':'c','ĉ':'c','ɕ':'c','ċ':'c','ƈ':'c','ȼ':'c','ď':'d','ḑ':'d','ḓ':'d','ȡ':'d','ḋ':'d','ḍ':'d','ɗ':'d','ᶑ':'d','ḏ':'d','ᵭ':'d','ᶁ':'d','đ':'d','ɖ':'d','ƌ':'d','ı':'i','ȷ':'j','ɟ':'j','ʄ':'j','ǳ':'dz','ǆ':'dz','é':'e','ĕ':'e','ě':'e','ȩ':'e','ḝ':'e','ê':'e','ế':'e','ệ':'e','ề':'e','ể':'e','ễ':'e','ḙ':'e','ë':'e','ė':'e','ẹ':'e','ȅ':'e','è':'e','ẻ':'e','ȇ':'e','ē':'e','ḗ':'e','ḕ':'e','ⱸ':'e','ę':'e','ᶒ':'e','ɇ':'e','ẽ':'e','ḛ':'e','ꝫ':'et','ḟ':'f','ƒ':'f','ᵮ':'f','ᶂ':'f','ǵ':'g','ğ':'g','ǧ':'g','ģ':'g','ĝ':'g','ġ':'g','ɠ':'g','ḡ':'g','ᶃ':'g','ǥ':'g','ḫ':'h','ȟ':'h','ḩ':'h','ĥ':'h','ⱨ':'h','ḧ':'h','ḣ':'h','ḥ':'h','ɦ':'h','ẖ':'h','ħ':'h','ƕ':'hv','í':'i','ĭ':'i','ǐ':'i','î':'i','ï':'i','ḯ':'i','ị':'i','ȉ':'i','ì':'i','ỉ':'i','ȋ':'i','ī':'i','į':'i','ᶖ':'i','ɨ':'i','ĩ':'i','ḭ':'i','ꝺ':'d','ꝼ':'f','ᵹ':'g','ꞃ':'r','ꞅ':'s','ꞇ':'t','ꝭ':'is','ǰ':'j','ĵ':'j','ʝ':'j','ɉ':'j','ḱ':'k','ǩ':'k','ķ':'k','ⱪ':'k','ꝃ':'k','ḳ':'k','ƙ':'k','ḵ':'k','ᶄ':'k','ꝁ':'k','ꝅ':'k','ĺ':'l','ƚ':'l','ɬ':'l','ľ':'l','ļ':'l','ḽ':'l','ȴ':'l','ḷ':'l','ḹ':'l','ⱡ':'l','ꝉ':'l','ḻ':'l','ŀ':'l','ɫ':'l','ᶅ':'l','ɭ':'l','ł':'l','ǉ':'lj','ſ':'s','ẜ':'s','ẛ':'s','ẝ':'s','ḿ':'m','ṁ':'m','ṃ':'m','ɱ':'m','ᵯ':'m','ᶆ':'m','ń':'n','ň':'n','ņ':'n','ṋ':'n','ȵ':'n','ṅ':'n','ṇ':'n','ǹ':'n','ɲ':'n','ṉ':'n','ƞ':'n','ᵰ':'n','ᶇ':'n','ɳ':'n','ñ':'n','ǌ':'nj','ó':'o','ŏ':'o','ǒ':'o','ô':'o','ố':'o','ộ':'o','ồ':'o','ổ':'o','ỗ':'o','ö':'o','ȫ':'o','ȯ':'o','ȱ':'o','ọ':'o','ő':'o','ȍ':'o','ò':'o','ỏ':'o','ơ':'o','ớ':'o','ợ':'o','ờ':'o','ở':'o','ỡ':'o','ȏ':'o','ꝋ':'o','ꝍ':'o','ⱺ':'o','ō':'o','ṓ':'o','ṑ':'o','ǫ':'o','ǭ':'o','ø':'o','ǿ':'o','õ':'o','ṍ':'o','ṏ':'o','ȭ':'o','ƣ':'oi','ꝏ':'oo','ɛ':'e','ᶓ':'e','ɔ':'o','ᶗ':'o','ȣ':'ou','ṕ':'p','ṗ':'p','ꝓ':'p','ƥ':'p','ᵱ':'p','ᶈ':'p','ꝕ':'p','ᵽ':'p','ꝑ':'p','ꝙ':'q','ʠ':'q','ɋ':'q','ꝗ':'q','ŕ':'r','ř':'r','ŗ':'r','ṙ':'r','ṛ':'r','ṝ':'r','ȑ':'r','ɾ':'r','ᵳ':'r','ȓ':'r','ṟ':'r','ɼ':'r','ᵲ':'r','ᶉ':'r','ɍ':'r','ɽ':'r','ↄ':'c','ꜿ':'c','ɘ':'e','ɿ':'r','ś':'s','ṥ':'s','š':'s','ṧ':'s','ş':'s','ŝ':'s','ș':'s','ṡ':'s','ṣ':'s','ṩ':'s','ʂ':'s','ᵴ':'s','ᶊ':'s','ȿ':'s','ɡ':'g','ᴑ':'o','ᴓ':'o','ᴝ':'u','ť':'t','ţ':'t','ṱ':'t','ț':'t','ȶ':'t','ẗ':'t','ⱦ':'t','ṫ':'t','ṭ':'t','ƭ':'t','ṯ':'t','ᵵ':'t','ƫ':'t','ʈ':'t','ŧ':'t','ᵺ':'th','ɐ':'a','ᴂ':'ae','ǝ':'e','ᵷ':'g','ɥ':'h','ʮ':'h','ʯ':'h','ᴉ':'i','ʞ':'k','ꞁ':'l','ɯ':'m','ɰ':'m','ᴔ':'oe','ɹ':'r','ɻ':'r','ɺ':'r','ⱹ':'r','ʇ':'t','ʌ':'v','ʍ':'w','ʎ':'y','ꜩ':'tz','ú':'u','ŭ':'u','ǔ':'u','û':'u','ṷ':'u','ü':'u','ǘ':'u','ǚ':'u','ǜ':'u','ǖ':'u','ṳ':'u','ụ':'u','ű':'u','ȕ':'u','ù':'u','ủ':'u','ư':'u','ứ':'u','ự':'u','ừ':'u','ử':'u','ữ':'u','ȗ':'u','ū':'u','ṻ':'u','ų':'u','ᶙ':'u','ů':'u','ũ':'u','ṹ':'u','ṵ':'u','ᵫ':'ue','ꝸ':'um','ⱴ':'v','ꝟ':'v','ṿ':'v','ʋ':'v','ᶌ':'v','ⱱ':'v','ṽ':'v','ꝡ':'vy','ẃ':'w','ŵ':'w','ẅ':'w','ẇ':'w','ẉ':'w','ẁ':'w','ⱳ':'w','ẘ':'w','ẍ':'x','ẋ':'x','ᶍ':'x','ý':'y','ŷ':'y','ÿ':'y','ẏ':'y','ỵ':'y','ỳ':'y','ƴ':'y','ỷ':'y','ỿ':'y','ȳ':'y','ẙ':'y','ɏ':'y','ỹ':'y','ź':'z','ž':'z','ẑ':'z','ʑ':'z','ⱬ':'z','ż':'z','ẓ':'z','ȥ':'z','ẕ':'z','ᵶ':'z','ᶎ':'z','ʐ':'z','ƶ':'z','ɀ':'z','ﬀ':'ff','ﬃ':'ffi','ﬄ':'ffl','ﬁ':'fi','ﬂ':'fl','ĳ':'ij','œ':'oe','ﬆ':'st','ₐ':'a','ₑ':'e','ᵢ':'i','ⱼ':'j','ₒ':'o','ᵣ':'r','ᵤ':'u','ᵥ':'v','ₓ':'x'};
			const accents = Object.keys(conversionMap).join('');
			return string.toString().trim().replace(new RegExp(`[^a-z0-9${accents} '.]`,'gi'),'');
		case "Aa":
			return Camelize(cleanString.toLowerCase()).replace(/[^a-z0-9]/gi,'');
		case "-Aa":
			return Camelize(cleanString.toLowerCase()).replace(/[^-a-z0-9]/gi,'');
		case "sanKey":
			return cleanString.replace(/[^-*_a-z0-9]/gi,"_").replace(/_+/g,"_").replace(/_$/g,"").replace(/^_/g,"");
	}
}
function camelize(str) {
	return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
		return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
	}).replace(/\s+/g, '');
}
function Camelize(str) {
	return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
		return letter.toUpperCase();
	}).replace(/\s+/g, '');
}
async function csv2yml(path,baseName="data") {
	const papa = require('papaparse');
	const rawCsv = await fs.readFile(`${path}/${baseName}.csv`, 'utf8');
	const papaCsv = await papa.parse(rawCsv,{header:true, dynamicTyping:true, skipEmptyLines:true});
	if(papaCsv.errors.length) console.error(papaCsv.errors, papaCsv.meta);
	const csv = papaCsv.data;
	csv.forEach((line)=>{
		for(let key in line){
			const oldKey = key;
			key = san(oldKey,"sanKey");
			if(key != oldKey){
				line[key] = line[oldKey];
				delete line[oldKey];
			}
			if(key === 'label') line[key] = san(line[key],' Aà');
			if(key === 'id') line[key] = san(line[key],'sanKey');

			if(key.indexOf('#') === -1) continue;
			const {newKey, sanType} = key.split('#');
			line[newKey] = san(line[key],sanType);
			delete line[key];
		}
	});

	csv.forEach((line)=>{
		for(let key in line){
			const oldKey = key;
			if(oldKey.indexOf('*')!==-1){
				key = oldKey.replace(/\*/g,'');
				if(key != oldKey){
					line[key] = line[oldKey];
					delete line[oldKey];
				}

				// cré le noeud s'il n'existe pas déjà
				const nodeId2 = `node-${key}-${san(line[key],'Aa').substr(0,20)}`;
				const existId2 = csv.findIndex((elem)=>elem.id===nodeId2);
				if(existId2 === -1) csv.push({id:nodeId2,label:line[key].toString().trim(),type:key});

				//cré le lien
				const linkId = `link-${key}-${san(line.label,'Aa').substr(0,20)}`;
				csv.push({id:linkId,source:line.id,target:nodeId2,label:key,type:key});

				if(oldKey.indexOf('**')!==-1){
					// cré le noeud s'il n'existe pas déjà
					const nodeId3 = `node-core-${key}`;
					const existId3 = csv.findIndex((elem)=>elem.id===nodeId3);
					if(existId3 === -1) csv.push({id:nodeId3,label:`core-${key}`,type:`core-${key}`});

					//cré le lien
					const linkId = `link-core-${key}-${san(line[key],'Aa').substr(0,20)}`;
					csv.push({id:linkId,source:nodeId2,target:nodeId3,label:`core-${key}`,type:`core-${key}`});
				}
			}
		}
	});


	await fs.ensureFile(`${path}/${baseName}.yml`);
	const target = await yaml.safeLoad(await fs.readFile(`${path}/${baseName}.yml`, 'utf8')) || {};
	for(let line of csv){
		if(!line || !line.id){
			console.error(`csv contain dirty data : `,line);
			continue;
		}
		const type = line.id.split('-')[0];
		if(!target[type])target[type] = [];
		const exist = target[type].findIndex((elem)=>elem.id===line.id);
		if(exist !== -1) {
			//TODO: merge au lieu d'overwrite
			target[type][exist] = line;
		} else target[type].push(line);
	}
	await fs.writeFile(`${path}/${baseName}.yml`,await yaml.safeDump(target));
	/*
	vérifier la présence d'une colonne label, la créer sinon en demandant la colonne à utiliser comme base.
	vérifier la présence d'une colonne id, la créer sinon à partir des label (en normalisant les données pour pouvoir les utiliser en id)

	extraire des noeud à partir des colonnes dont l'identifiant fini par * (avant l'éventuel #) id et label basé sur la valeur du champ + type avec intitulé de colonne.
	créer des liens entre les noeud d'origine et les noeud extraits
	si l'identifiant fini par ** créer un noeud central lié au noeuds extraits.

	afficher tout les conflit, bug et autres truc foireux non géré trouvé dans le fichier d'origine.
	 */
}
const publicFunc = {help,init,update,build,start,contribAuto,csv2yml,version};
publicFunc['--version'] = version;
publicFunc['-v'] = version;
publicFunc['--help'] = help;
publicFunc['-h'] = help;
module.exports = publicFunc;
